<?php

namespace GeraldoLabs\Router;

require __DIR__ . '/functions.php';

spl_autoload_register(function($class) {
    if (strpos($class, 'GeraldoLabs\\Router\\') === 0) {
        $name = substr($class, strlen('GeraldoLabs\\Router\\'));
        require __DIR__ . DIRECTORY_SEPARATOR . strtr($name, '\\', DIRECTORY_SEPARATOR) . '.php';
    }
});
